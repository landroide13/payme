class Payment {
  constructor(date, hours, rate){
    this.date = date,
    this.hours = hours,
    this.rate = rate
  }
} 
  
// UI Class
class UI {

  static displayPayments() {
    const payments = Store.getPayments();
    payments.forEach(payment => UI.addPaymentToList(payment))
  }

  static addPaymentToList(payment) {
    const paymentList = document.querySelector('#payment-list');
  
    const row = document.createElement('div')
    row.classList.add('cell')

    const ammount = payment.hours * payment.rate

    const tax = ammount * 0.2

    const aft = ammount - tax 

    row.innerHTML = `
      <div class="td">${payment.hours}</div>
      <div class="td"><span class="sym">NZ</span> ${payment.rate}</div>
      <div class="td"><span class="sym">NZ</span> ${Math.trunc(tax)}</div>
      <div class="td"><span class="sym">NZ</span> ${Math.trunc(aft)}</div>
      <div class="td">${payment.date}</div>
      <div class="td"><button href="#" class="delete">X</button></div>
    `;

    paymentList.appendChild(row);
  }

  static displayTotals() {
    
    const payments = Store.getPayments();
    const totalPay = [];
    const totalTax = [];

    payments.forEach(payment => {
      const ammount = payment.hours * payment.rate;
      const tax = Math.trunc(ammount * 0.2);
      totalTax.push(tax);
      const total = Math.trunc(ammount - tax);
      totalPay.push(total);
    })
    const aft = totalPay.reduce((a, b) => a + b, 0);
    const tax = totalTax.reduce((a, b) => a + b, 0);
    console.log("Net Ammount: " + aft)
    console.log("Total Taxes: " + tax)

    const totalList = document.querySelector('#total');
    const template1 = document.createElement("div");
    const template2 = document.createElement("div");

    template1.innerHTML = `
          <div class="total">
            <div><span>Net Pay: NZ </spa>${ aft }</div>
            <div><span>Total Deductions: NZ </spa>${ tax }</div>
          </div>
          `
    template2.innerHTML = `
          <div class="total">
            <div><span>Net Pay: NZ </span>0</div>
            <div><span>Total Deductions: NZ </span>0</div>
          </div>
          `
    if (aft !== 0) {
        totalList.appendChild(template1);
      } else {
        totalList.appendChild(template2);
      }

    
  }

  static deletePayment(el) {
    if (el.classList.contains('delete')) {
      el.parentElement.parentElement.remove();
    }
  }

  static clearFields() {
    document.querySelector('#hours').value = '';
    document.querySelector('#rate').value = '';
    document.querySelector("#date").value = "";
  }

}

///Store Payments

class Store {

  static getPayments() {
    let payments;
    if (localStorage.getItem('payments') === null) {
      payments = []
    } else {
      payments = JSON.parse(localStorage.getItem('payments'));
    }
    return payments;
  }

  static addPayment(payment) {
    const payments = Store.getPayments();
    payments.push(payment);
    localStorage.setItem('payments', JSON.stringify(payments));
  }

  static removePayment(date) {
    // console.log(date)
    const payments = Store.getPayments();
    payments.forEach((payment, idx) => {
      if(payment.date === date){
        console.log("Payments " + payment.date);
        payments.splice(idx, 1);
      }
    })
    localStorage.setItem('payments', JSON.stringify(payments))
  }

}
//Events diplsay Payments
document.addEventListener('DOMContentLoaded', UI.displayPayments());

//Events display Totals
document.addEventListener("DOMContentLoaded", UI.displayTotals());

//Event Add Payment
document.querySelector('#payment-form').addEventListener('submit', e => {
  //Prevent Default
  e.preventDefault();
  //Get Values from the form
  
  const date = document.querySelector('#date').value;
  const shortDate = date.slice(5)
  const hours = document.querySelector('#hours').value;
  const rate = document.querySelector('#rate').value;

  //Validates Fields
  if (date === '' || hours === '' || rate === '') {
    UI.showAlert('Please fill in all the fields..', 'danger');

  } else {

    //Instance the Object
    const payment = new Payment(shortDate, hours, rate);
    
    //Add Pay to UI
    UI.addPaymentToList(payment);

    //Add Pay to Local Storage
    Store.addPayment(payment);
    
    //Clear Fields
    UI.clearFields();
  }
})

//Event Delete Line
document.querySelector('#payment-list').addEventListener('click', e => {
  UI.deletePayment(e.target);
  Store.removePayment(e.target.parentElement.previousElementSibling.textContent)
});












